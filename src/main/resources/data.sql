REPLACE INTO oauth_client_details
(client_id, client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity,
 refresh_token_validity, additional_information, autoapprove)
VALUES
('web', '$2a$10$p60LHtCWCK41Kry7H2SGhuON6ghyyeguH/1CFqm4BORrL/XwndSeC', 'read,write,trust',
 'password,refresh_token', null, null, 60, 36000, null, false);
REPLACE INTO oauth_client_details
(client_id, client_secret, scope, authorized_grant_types,
 web_server_redirect_uri, authorities, access_token_validity,
 refresh_token_validity, additional_information, autoapprove)
VALUES
('web1', '$2a$10$p60LHtCWCK41Kry7H2SGhuON6ghyyeguH/1CFqm4BORrL/XwndSeC', 'read,write,trust',
 'password,implicit,access_token', null, null, 900, 36000, null, false);
REPLACE INTO USERS(ID,USERNAME,PASSWORD)
VALUE
(
'9999998','test','$2a$08$fL7u5xcvsZl78su29x1ti.dxI.9rYO8t0q5wk2ROJ.1cdR53bmaVG'
);
REPLACE INTO USERS(ID,USERNAME,PASSWORD)
VALUE
(
'9999999','test2','$2a$08$fL7u5xcvsZl78su29x1ti.dxI.9rYO8t0q5wk2ROJ.1cdR53bmaVG'
);