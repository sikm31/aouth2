package ua.concord.oauth2.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import ua.concord.oauth2.service.UserService;

import javax.sql.DataSource;

/**
 * The type Oauth 2 configuration.
 */
@Configuration
@EnableAuthorizationServer
public class Oauth2Configuration extends AuthorizationServerConfigurerAdapter {

	private final AuthenticationManager authenticationManager;

	private final DataSource dataSource;

	private final PasswordEncoder encoder;

	private final UserService userService;

	public Oauth2Configuration(@Qualifier("authenticationManagerBean") AuthenticationManager authenticationManager,
							   @Qualifier("dataSourceMySql") DataSource dataSource,
							   PasswordEncoder encoder,
							   UserService userService) {
		this.authenticationManager = authenticationManager;
		this.dataSource = dataSource;
		this.encoder = encoder;
		this.userService = userService;
	}

	@Value("${token-jti}")
	private boolean tokenJti;

	@Bean
	public ClientCredentialsTokenEndpointFilter checkTokenEndpointFilter() {
		ClientCredentialsTokenEndpointFilter filter = new ClientCredentialsTokenEndpointFilter("/oauth/check_token");
		filter.setAuthenticationManager(authenticationManager);
		filter.setAllowOnlyPost(true);
		return filter;
	}

	@Bean
	public TokenStore tokenStore() {
		return new JdbcTokenStore(dataSource);
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		return new JwtAccessTokenConverter();
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
		configurer.jdbc(dataSource).passwordEncoder(encoder);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		if (tokenJti) {
			endpoints.tokenStore(tokenStore()).authenticationManager(authenticationManager)
					.accessTokenConverter(accessTokenConverter()).userDetailsService(userService);
		} else {
			endpoints.tokenStore(tokenStore()).authenticationManager(authenticationManager).userDetailsService(userService);
		}
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
		oauthServer
				.allowFormAuthenticationForClients()
				.checkTokenAccess("permitAll()")
				.addTokenEndpointAuthenticationFilter(checkTokenEndpointFilter());
	}

}
