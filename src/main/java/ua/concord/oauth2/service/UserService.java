package ua.concord.oauth2.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.concord.oauth2.dao.CustomUser;
import ua.concord.oauth2.dao.OauthDao;

/**
 * The type User service.
 */
@Log4j2
@Service
public class UserService implements UserDetailsService {

	private final OauthDao oauthDao;

	/**
	 * Instantiates a new User service.
	 *
	 * @param oauthDao the oauth dao
	 */
	public UserService(OauthDao oauthDao) {
		this.oauthDao = oauthDao;
	}

	@Override
	public CustomUser loadUserByUsername(String username) throws UsernameNotFoundException {

		try {
			return new CustomUser(oauthDao.getUserDetails(username));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new UsernameNotFoundException("User " + username + " was not found in the database");
		}
	}
}
