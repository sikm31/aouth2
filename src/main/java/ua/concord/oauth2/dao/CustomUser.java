package ua.concord.oauth2.dao;

import org.springframework.security.core.userdetails.User;
import ua.concord.oauth2.entity.UserEntity;

public class CustomUser extends User {

	private static final long serialVersionUID = 1L;
	public CustomUser(UserEntity user) {
		super(user.getUsername(), user.getPassword(), user.getGrantedAuthoritiesList());
	}
}
