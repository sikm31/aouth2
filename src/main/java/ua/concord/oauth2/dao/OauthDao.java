package ua.concord.oauth2.dao;

import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import ua.concord.oauth2.entity.UserEntity;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.Collection;

/**
 * The type Oauth dao.
 */
@Log4j2
@Repository
public class OauthDao {

	private final JdbcTemplate jdbcTemplate;

	public OauthDao(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * Gets user details.
	 *
	 * @param username the username
	 * @return the user details
	 */
	public UserEntity getUserDetails(String username) {
		Collection<GrantedAuthority> grantedAuthoritiesList = Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
		String sql = "SELECT * FROM USERS WHERE USERNAME=?";
		try {
			UserEntity userEntity = jdbcTemplate.queryForObject(sql, new String[]{username}, (ResultSet rs, int rowNum) -> {
				UserEntity user = UserEntity.builder()
						.id(rs.getInt("ID"))
						.username(username)
						.password(rs.getString("PASSWORD"))
						.grantedAuthoritiesList(grantedAuthoritiesList)
						.build();
				return user;
			});
			return userEntity;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new UsernameNotFoundException("User " + username + " was not found in the database");
		}
	}

}
