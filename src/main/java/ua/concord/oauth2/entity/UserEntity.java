package ua.concord.oauth2.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * The type User entity.
 */
@Data
@Builder
public class UserEntity {

	private Integer id;
	private String username;
	private String password;
	private Collection<GrantedAuthority> grantedAuthoritiesList;
}
